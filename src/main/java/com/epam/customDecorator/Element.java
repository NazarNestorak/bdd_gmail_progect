package com.epam.customDecorator;

import org.openqa.selenium.WebElement;

public class Element {
    private WebElement webElement;

    public Element(WebElement webElement) {
        this.webElement = webElement;
    }

    public WebElement getWebElement() {
        return webElement;
    }
}
