package com.epam.gmailBO;

import com.epam.model.Letter;
import com.epam.gmailPO.GmailHomePage;
import com.epam.gmailPO.LoginPage;
import com.epam.gmailPO.PasswordPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GmailPageBO {
    private static Logger logger = LogManager.getLogger(GmailPageBO.class);
    private GmailHomePage gmailHomePage;
    private LoginPage loginPage;
    private PasswordPage passwordPage;
    private Letter letter;

    public GmailPageBO() {
        letter = new Letter();
        gmailHomePage = new GmailHomePage();
        loginPage = new LoginPage();
        passwordPage = new PasswordPage();
    }

    public void logging(String login, String password) {
        logger.info("Fill in the 'user login' field ");
        loginPage.getEmailInput().sendKeys(login);
        logger.info("Pressing the button to confirm 'user login' and switching to 'PasswordPage'");
        loginPage.getConfirmEmailButton().clickIfButtonClickable();
        logger.info("Fill in the 'user password' field ");
        passwordPage.getPasswordInput().sendKeys(password);
        logger.info("Pressing the button to confirm 'user password' and switching to 'GmailHomePage'");
        passwordPage.getPasswordNextButton().clickIfButtonClickable();
    }

    public void writeAndSendLetter(Letter letter) {
        logger.info("Click 'Compose' button");
        gmailHomePage.getComposeButton().clickIfButtonClickable();
        logger.info("Fill in field 'Recipient'");
        gmailHomePage.getFieldRecipient().sendKeys(letter.getRecipient());
        logger.info("Fill in field 'Subject'");
        gmailHomePage.getFieldSubject().sendKeys(letter.getSubject());
        logger.info("Fill in field 'Text'");
        gmailHomePage.getTextField().sendKeys(letter.getText());
        logger.info("Click 'Send Message' button");
        gmailHomePage.getSendLetterButton().clickIfButtonClickable();
    }

    public void openLastSendMessage() throws InterruptedException {
        logger.info("Open folder with all sent message");
        gmailHomePage.getSendFolder().clickIfButtonClickable();
        logger.info("Select the message that was last sent");
        Thread.sleep(5000);
        gmailHomePage.getLastSentMessage().click();
    }

    public String  getTitle() {
        return gmailHomePage.getLetterTitle().getText();
    }

}
