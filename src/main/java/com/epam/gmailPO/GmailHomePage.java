package com.epam.gmailPO;

import com.epam.customDecorator.Button;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailHomePage extends PageObject {

    @FindBy(css = "div.T-I.J-J5-Ji.T-I-KE.L3")
    private Button composeButton;

    @FindBy(css = "textarea.vO[name='to']")
    private WebElement fieldRecipient;

    @FindBy(xpath= "//input[@name='subjectbox']")
    private WebElement fieldSubject;

    @FindBy(xpath = "//div[@role= 'textbox']")
    private WebElement textField;

    @FindBy(css = "div.T-I.J-J5-Ji.aoO.v7.T-I-atl.L3")
    private Button sendLetterButton;

    @FindBy(xpath = "//div[@class='TO']//a[@href='https://mail.google.com/mail/u/0/#sent']")
    private Button sendFolder;

    @FindBy(xpath = "(//div[@role='main']//table/tbody/tr[1]//span[@class='y2'])")
    private WebElement lastSentMessage;

    @FindBy(xpath="div[@class='G-tF']//div[@role='button']/div[@class='asa']")
    private Button deleteMessageButton;

    @FindBy(xpath = "//div[@class='AO']//div[@role='main']//div[@class='ha']/h2[@class='hP']")
    private WebElement letterTitle;

    public Button getComposeButton() {
        return composeButton;
    }

    public WebElement getFieldRecipient() {
        return fieldRecipient;
    }

    public WebElement getFieldSubject() {
        return fieldSubject;
    }

    public WebElement getTextField() {
        return textField;
    }

    public Button getSendLetterButton() {
        return sendLetterButton;
    }

    public Button getSendFolder() {
        return sendFolder;
    }

    public WebElement getLastSentMessage() {
        return lastSentMessage;
    }

    public WebElement getLetterTitle() {
        return letterTitle;
    }

}
