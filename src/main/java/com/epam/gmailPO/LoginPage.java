package com.epam.gmailPO;

import com.epam.customDecorator.Button;
import com.epam.helper.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.helper.Constanta.GMAIL_LOGIN_PAGE_URL;

public class LoginPage extends PageObject {

    @FindBy(id = "identifierId")
    private WebElement emailInput;

    @FindBy(id = "identifierNext")
    private Button confirmEmailButton;


    public WebElement getEmailInput() {
        return emailInput;
    }

    public Button getConfirmEmailButton() {
        return confirmEmailButton;
    }
}
