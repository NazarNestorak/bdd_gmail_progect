package com.epam.gmailPO;

import com.epam.customDecorator.CustomFieldDecorator;
import com.epam.helper.DriverProvider;
import org.openqa.selenium.support.PageFactory;

public class PageObject {

    public PageObject() {
        PageFactory.initElements(new CustomFieldDecorator(DriverProvider.getDriver()),this);
    }
}

