package com.epam.gmailPO;

import com.epam.customDecorator.Button;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PasswordPage extends PageObject {

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(id = "passwordNext")
    private Button passwordNextButton;

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public Button getPasswordNextButton() {
        return passwordNextButton;
    }
}
