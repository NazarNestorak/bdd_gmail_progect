package com.epam.helper;

public class Constanta {
    public static final  int WAIT_TIME_OUT = 90;
    public static final  int WAIT_SLEEP_TIME = 1500;
    public static final String RECIPIENT = "nestoraknazik@gmail.com";
    public static final String SUBJECT = "Argent letter";
    public static final String TEXT = "It is trial letter";
    public static final String GMAIL_LOGIN_PAGE_URL = "https://accounts.google.com/ServiceLogin/signinchooser?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin";


    private Constanta() {
    }
}
