Feature: Gmail send message

  Scenario Outline: Create, send message and verify mailbox
    When I LogIn my personal page by my <login> and <password>
    And I create new message and send it
    And I open last sent message into Sent folder
    Then I verify if my message was sent

    Examples:
    |          login         |         password        |
    |nazarnestorak@gmail.com |may the force be with you|
    |nazarnestorak1@gmail.com|may the force be with you|
    |nazarnestorak2@gmail.com|may the force be with you|
    |nazarnestorak3@gmail.com|may the force be with you|