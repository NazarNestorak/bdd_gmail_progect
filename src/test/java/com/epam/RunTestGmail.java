package com.epam;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features ="src/main/resources/gmail.feature", glue = {"com.epam.gmailPO"})
public class RunTestGmail{
}
