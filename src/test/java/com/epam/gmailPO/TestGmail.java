package com.epam.gmailPO;

import com.epam.gmailBO.GmailPageBO;
import com.epam.helper.DriverProvider;
import com.epam.model.Letter;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import static com.epam.helper.Helper.*;

public class TestGmail {
    private GmailPageBO gmailPageBO;
    private Letter letter;

    @Before
    public void initialize() {
        System.setProperty(WEBDRIVER, WEBDRIVER_PATH);
        letter = new Letter();
        DriverProvider.getDriver().get(GMAIL_LOGIN_PAGE_URL);
    }

    @When("^I LogIn my personal page by my ([^\"]*) and ([^\"]*)$")
    public void loginByAcount(String login, String password) {
        gmailPageBO = new GmailPageBO();
        gmailPageBO.logging(login,password);
    }


    @And("^I create new message and send it$")
    public void createAndSendMessage() {
        GmailPageBO gmailPageBO = new GmailPageBO();
        gmailPageBO.writeAndSendLetter(letter);
    }

    @And("^I open last sent message into Sent folder$")
    public void openLastSentMessage() throws InterruptedException {
        gmailPageBO.openLastSendMessage();
    }

    @Then("^I verify if my message was sent$")
    public void checkIfMessageWasSent() {
        Assert.assertEquals(letter.getSubject().equals(gmailPageBO.getTitle()),"not find needed letter");
    }

    @After
    public void closeDriver() {
        DriverProvider.closeDriver();
    }

}
